from typing import List, Tuple, Optional, Iterator

import asyncio
import aiohttp

API_URL = "https://world-population.p.rapidapi.com/population"


API_KEY = "YOUR_KEY"
HEADERS = {
    "X-RapidAPI-Host": "world-population.p.rapidapi.com",
    "X-RapidAPI-Key": API_KEY,
    "Content-Type": "application/json",
}

async def fetch_population(session: aiohttp.ClientSession, country: str) -> Optional[Tuple[str, int]]:
    params = {"country_name": country}
    try:
        async with session.get(API_URL, headers=HEADERS, params=params) as response:
            if response.status == 429:
                print(f"Rate limit exceeded for {country}. Waiting for a while.")
                await asyncio.sleep(5)  
                return await fetch_population(session, country)
            
            if response.status == 403:
                print(f"Forbidden for {country}. Check API key and permissions.")
                return None

            response.raise_for_status()
            data = await response.json()
            population = data['body']['population']
            return country, population
    except aiohttp.ClientResponseError as e:
        print(f"Error fetching data for {country}: {e}")
        return None

async def fetch_all_populations(countries: List[str]) -> List[Optional[Tuple[str, int]]]:
    async with aiohttp.ClientSession() as session:
        tasks = [fetch_population(session, country) for country in countries]
        results = await asyncio.gather(*tasks)
        return results

def filter_population(results: List[Tuple[str, int]], threshold: int) -> Iterator[Tuple[str, int]]:
    filtered_results = filter(None, results)  
    return filter(lambda x: x[1] >= threshold, filtered_results)

def process_population(results: Iterator[Tuple[str, int]]) -> Iterator[Tuple[str, int]]:
    return map(lambda x: (x[0], x[1] * 2), results)

def main():
    countries: List[str] = ["China", "India", "United States", "Indonesia", "Pakistan"]
    population_threshold: int = 100000000  

    loop = asyncio.get_event_loop()
    results: List[Optional[Tuple[str, int]]] = loop.run_until_complete(fetch_all_populations(countries))

    filtered_results: Iterator[Tuple[str, int]] = filter_population(results, population_threshold)
    processed_results: Iterator[Tuple[str, int]] = process_population(filtered_results)

    for result in processed_results:
        print(f"Processed: {result[0]} - {result[1]}")

if __name__ == "__main__":
    main()
